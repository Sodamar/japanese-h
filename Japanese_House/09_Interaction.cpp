/*
* Laboratorio de Computación Gráfica e Interacción Humano-Computadora
* 05 - Carga de modelos e interacción
*/

#include <iostream>
#include <stdlib.h>

// GLAD: Multi-Language GL/GLES/EGL/GLX/WGL Loader-Generator
// https://glad.dav1d.de/
#include <glad/glad.h>

// GLFW: https://www.glfw.org/
#include <GLFW/glfw3.h>

// GLM: OpenGL Math library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Model loading classes
#include <shader_m.h>
#include <camera.h>
#include <model.h>
#include <material.h>
#include <light.h>
#include <cubemap.h>
#include <iostream>

//Audio
#include <irrKlang.h>
using namespace irrklang;

// Max number of bones
#define MAX_RIGGING_BONES 4

// Functions
bool Start();
bool Update();

// Definición de callbacks
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

// Gobals
GLFWwindow* window;

// Tamaño en pixeles de la ventana
const unsigned int SCR_WIDTH = 1024;
const unsigned int SCR_HEIGHT = 768;

// Definición de cámara (posición en XYZ)
Camera camera(glm::vec3(2.0f, 1.5f, -2.0f)); //Camera camera(glm::vec3(14.0f, 1.0f, -40.0f)); 

// Controladores para el movimiento del mouse
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// Variables para la velocidad de reproducción
// de la animación
float deltaTime = 0.0f;
float lastFrame = 0.0f;
float elapsedTime = 0.0f;

glm::vec3 position(0.0f,0.0f, 0.0f);
glm::vec3 forwardView(0.0f, 0.0f, 1.0f);
float     scaleV = 0.005f;
float     rotateCharacter = 0.0f;
float     rotateTable = 0.0f;


// Shaders
// Shaders
Shader* ourShader;
Shader* staticShader;
Shader* phongShader;
Shader* fresnelShader;
Shader* cubemapShader;

// Carga la información del modelo
Model* character;
Model* Barda;
Model* sakura;
Model* paredes;
Model* cama_03;
Model* maceta;
Model* techo;
Model* paisaje;
Model* conjuntoR;
Model* conjuntoC;

//-------------
//Elementos separados por los efectos de iluminación
Model* Casa_te;
Model* Piso_ext;
Model* Puente;
Model* lampara;
Model* lampara02;
//-------------
Model* Pisos_int;
Model* Banco_B;
Model* petalo;
Model* agua;
Model* karpa;
//Model* Bonsai;

// Cubemap
//CubeMap* mainCubeMap;

//Animation vars
float tradius = 9.0f;
float theta = 0.0f;

// Materiales
Material material01;
Material vidrio_lamp;
Material wood;
Material wall;
Light    light01;
Light	 lamp;
Light	 lamp02;
Light	 sunset;
Light	 sunset_piso;
Light	 W_light;

// Pose inicial del modelo
glm::mat4 gBones[MAX_RIGGING_BONES];
glm::vec3 BoundingBoxMin(0.0f, 0.0f, 0.0f);
glm::vec3 BoundingBoxMax(31.0f, 0.0f, -55.2f);
float	fps = 0.0f;
int		keys = 0;
int		animationCount = 0;

//Animation
glm::vec3 positionP(20.0f, 3.0f, -9.0f);
glm::vec3 force(0.0f, 0.0f, 0.0f);
glm::vec3 acceleration(0.0f, 0.0f, 0.0f);
glm::vec3 velocity(0.0f, 0.0f, 0.0f);
glm::vec3 gravity(0.0f, -9.81f, 0.0f);
float mass_p = 0.0001f;
float mass = 0.01f;


//-----------------AUDIO-----------------------------------
//
//--------------Posición de sonido--------------------------
vec3df i_position(0.0f, 0.0f, 0.0f);
vec3df position_1(6.0f, 1.0f, -8.0f); //Cuarto de lavado
vec3df position_2(6.0f, 1.0f, -15.0f); //Recámara 1
vec3df position_3(25.0f, 1.0f, -8.0f); //Recámara 2
vec3df position_4(25.0f, 1.0f, -18.0f); //Recámara 3
vec3df position_5(14.0f, 1.0f, -40.0f); //Casa del té
//----------------------------------------------------------

float volume = 0.4;
bool button = false;
vec3df cam_position(2.0f, 1.5f, -2.0f);
vec3df cam_Front(0.0f, 0.0f, 0.0f);
ISoundEngine* soundEngine = createIrrKlangDevice();
ISoundEngine* recordEngine = createIrrKlangDevice();
ISound* snd = soundEngine->play3D("audio/Mizuki.mp3", i_position, true, true);
IAudioRecorder* recorder = createIrrKlangAudioRecorder(recordEngine);
//-----------------------------------------------------------

// Entrada a función principal
int main()
{

	if (!Start())
		return -1;
	//Audio
	if (!soundEngine || !recorder) {
		return 0; // error starting up the engine
	}

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		if (!Update())
			break;
	}

	//Irrklang: Liberamos memoria
	if (snd)
	{
		snd->drop();
		recorder->drop();
		snd = 0;
	}
	
	//soundEngine->drop();
	glfwTerminate();
	return 0;
}

bool Start() {
	// Inicialización de GLFW

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Creación de la ventana con GLFW
	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Japanese House", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return false;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// Ocultar el cursor mientras se rota la escena
	// glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// glad: Cargar todos los apuntadores
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return false;
	}

	//Materials
	material01.ambient = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	material01.diffuse = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
	material01.specular = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);
	material01.transparency = 0.4f;

	vidrio_lamp.ambient = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	vidrio_lamp.diffuse = glm::vec4(0.6f, 0.5f, 0.6f, 1.0f);
	vidrio_lamp.specular = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);
	vidrio_lamp.transparency = 1.0f;

	wood.ambient = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f); //0.271484375f, 0.337890625f, 0.390625f,1.0f
	wood.diffuse = glm::vec4(0.6f, 0.5f, 0.6f, 1.0f);//1.0f, 1.0f, 0.654901f, 1.0f
	wood.specular = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);//0.0f, 0.0f, 0.0f, 1.0f
	wood.transparency = 1.0f;

	wall.ambient = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f); //0.271484375f, 0.337890625f, 0.390625f,1.0f
	wall.diffuse = glm::vec4(0.6f, 0.5f, 0.6f, 1.0f);//1.0f, 1.0f, 0.654901f, 1.0f
	wall.specular = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);//0.0f, 0.0f, 0.0f, 1.0f
	wall.transparency = 1.0f;

	//Lights
	lamp.Position = glm::vec3(14.5f, 2.0f, -37.6f); // Posición de la fuente de luz
	lamp.Direction = glm::vec3(1.0f, 0.7f, 1.0f); // Dirección (no aplica para fuentes puntuales)
	lamp.Color = glm::vec4(0.9725f, 0.8274f, 0.090f, 1.0f); // Color de la fuente de luz
	lamp.Power = glm::vec4(6.0f, 6.0f, 6.0f, 1.0f); // Potencia en Watts
	lamp.alphaIndex = 0.7; // potencia del brillo especular
	lamp.distance = 1.5f;

	lamp02.Position = glm::vec3(19.0f, 2.0f, -37.6f);
	lamp02.Direction = glm::vec3(1.0f, 0.7f, 1.0f); 
	lamp02.Color = glm::vec4(0.9725f, 0.8274f, 0.090f, 1.0f);
	lamp02.Power = glm::vec4(6.0f, 6.0f, 6.0f, 1.0f); 
	lamp02.alphaIndex = 0.7;
	lamp02.distance = 1.5f;

	sunset.Position = glm::vec3(40.0f, 2.0f, -60.0f); //100.0f, 10.0f, -90.0f  30.0f, -30.0f, -40.0f
	sunset.Direction = glm::vec3(1.0f, 0.0f, 1.0f); 
	sunset.Color = glm::vec4(0.9725f, 0.54901f, 0.0f, 1.0f); //0.368, 0.368, 0.431, 1.0f
	sunset.Power = glm::vec4(3.0f, 3.0f, 3.0f, 1.0f); 
	sunset.alphaIndex = 0.1; 
	sunset.distance = 2.0f;

	sunset_piso.Position = glm::vec3(-40.0f, -2.0f, 60.0f); //100.0f, 10.0f, -90.0f  30.0f, -30.0f, -40.0f
	sunset_piso.Direction = glm::vec3(1.0f, 0.0f, 1.0f);
	sunset_piso.Color = glm::vec4(0.9725f, 0.54901f, 0.0f, 1.0f); //0.368, 0.368, 0.431, 1.0f
	sunset_piso.Power = glm::vec4(3.0f, 3.0f, 3.0f, 1.0f);
	sunset_piso.alphaIndex = 0.1;
	sunset_piso.distance = 1.8f;

	W_light.Position = glm::vec3(14.0f,-40.0f, -10.5f); //100.0f, 10.0f, -90.0f  30.0f, -30.0f, -40.0f
	W_light.Direction = glm::vec3(1.0f, 1.0f, 1.0f);
	W_light.Color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f); //0.368, 0.368, 0.431, 1.0f
	W_light.Power = glm::vec4(1.5f,1.5f,1.5f, 0.5f);
	W_light.alphaIndex = 0.1;
	W_light.distance = 1.5f;

	
	// Activación de buffer de profundidad
	glEnable(GL_DEPTH_TEST);

	// Compilación y enlace de shaders
	ourShader = new Shader("shaders/10_vertex_skinning-IT.vs", "shaders/10_fragment_skinning-IT.fs");
	staticShader = new Shader("shaders/10_vertex_simple.vs", "shaders/10_fragment_simple.fs");
	phongShader = new Shader("shaders/11_BasicPhongShader.vs", "shaders/11_BasicPhongShader.fs");
	//fresnelShader = new Shader("shaders/11_Fresnel.vs", "shaders/11_Fresnel.fs");
	cubemapShader = new Shader("shaders/10_vertex_cubemap.vs", "shaders/10_fragment_cubemap.fs");

	// Máximo número de huesos: 100
	ourShader->setBonesIDs(MAX_RIGGING_BONES);

	// Dibujar en malla de alambre
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	Barda = new Model("models/interaction/Barda.fbx");
	sakura = new Model("models/interaction/sakura.fbx");

	character = new Model("models/interaction/KarpaKoi.fbx");
	paredes = new Model("models/interaction/Paredes.fbx");
	cama_03 = new Model("models/interaction/Cama.fbx");
	maceta = new Model("models/interaction/maceta.fbx");
	techo = new Model("models/interaction/Techo.fbx");
	paisaje = new Model("models/interaction/Paisaje.fbx");
	conjuntoR = new Model("models/interaction/Conjunto-Rmxta.fbx");
	conjuntoC = new Model("models/interaction/Conjunto-Rmxta.fbx");
	//-------Modelos por separado debido a los efectos de iluminación-----
	Casa_te = new Model("models/interaction/Casa_te.fbx");
	Puente = new Model("models/interaction/Puente.fbx");
	Piso_ext = new Model("models/interaction/Piso_ext.fbx");
	lampara = new Model("models/interaction/Lamp_ext.fbx");
	lampara02 = new Model("models/interaction/Lamp_ext02.fbx");
	//-------------------------------------------------------------------
	Pisos_int = new Model("models/interaction/Pisos_interiores.fbx");
	Banco_B = new Model("models/interaction/Banco_B.fbx");
	petalo = new Model("models/interaction/Flower.fbx");
	agua = new Model("models/interaction/Agua.fbx");
	karpa = new Model("models/interaction/KarpaKoi.fbx");

	// time, arrays
	character->SetPose(0.0f, gBones);

	fps = (float)character->getFramerate();
	keys = (int)character->getNumFrames();

	//Sound
	if (snd) {
		//soundEngine->setDefault3DSoundMinDistance(0.7f); NO
		//soundEngine->setDefault3DSoundMaxDistance(0.9f); NO
		snd->setVolume(volume);
		//snd->setMinDistance(0.15f);
		//snd->setMaxDistance(200.0f);NO
		//soundEngine->setRolloffFactor(10.0);NO
		//snd->setPlaybackSpeed(2.0f);NO
		snd->setIsPaused(false);
	}



	return true;
}

bool Update() {
	// Cálculo del framerate
	float currentFrame = (float)glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;

	elapsedTime += deltaTime;
	if (elapsedTime > 1.0f / fps) {
		animationCount++;
		if (animationCount > keys - 1) {
			animationCount = 0;
		}
		// Configuración de la pose en el instante t
		character->SetPose((float)animationCount, gBones);
		elapsedTime = 0.0f;
	}

	// Procesa la entrada del teclado o mouse
	processInput(window);

	// Renderizado R - G - B - A
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Dibujamos la lámpara exterior 01
	{
		// Activamos el shader estático
		phongShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", lamp.Color);
		phongShader->setVec4("LightPower", lamp.Power);
		phongShader->setInt("alphaIndex", lamp.alphaIndex);
		phongShader->setFloat("distance", lamp.distance);
		phongShader->setVec3("lightPosition", lamp.Position);
		phongShader->setVec3("lightDirection", lamp.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", vidrio_lamp.ambient);
		phongShader->setVec4("MaterialDiffuseColor", vidrio_lamp.diffuse);
		phongShader->setVec4("MaterialSpecularColor", vidrio_lamp.specular);
		phongShader->setFloat("transparency", vidrio_lamp.transparency);

		lampara->Draw(*phongShader);

	}

	glUseProgram(0);

	// Dibujamos la lámpara exterior 02
	{
		// Activamos el shader estático
		phongShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", lamp02.Color);
		phongShader->setVec4("LightPower", lamp02.Power);
		phongShader->setInt("alphaIndex", lamp02.alphaIndex);
		phongShader->setFloat("distance", lamp02.distance);
		phongShader->setVec3("lightPosition", lamp02.Position);
		phongShader->setVec3("lightDirection", lamp02.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", vidrio_lamp.ambient);
		phongShader->setVec4("MaterialDiffuseColor", vidrio_lamp.diffuse);
		phongShader->setVec4("MaterialSpecularColor", vidrio_lamp.specular);
		phongShader->setFloat("transparency", vidrio_lamp.transparency);

		lampara02->Draw(*phongShader);

	}

	glUseProgram(0);

	// Dibujamos el agua
	{
		// Activamos el shader estático
		phongShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		phongShader->setVec4("LightColor", sunset_piso.Color);
		phongShader->setVec4("LightPower", sunset_piso.Power);
		phongShader->setInt("alphaIndex", sunset_piso.alphaIndex);
		phongShader->setFloat("distance", sunset_piso.distance);
		phongShader->setVec3("lightPosition", sunset_piso.Position);
		phongShader->setVec3("lightDirection", sunset_piso.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", material01.ambient);
		phongShader->setVec4("MaterialDiffuseColor", material01.diffuse);
		phongShader->setVec4("MaterialSpecularColor", material01.specular);
		phongShader->setFloat("transparency", material01.transparency);

		agua->Draw(*phongShader);

	}

	glUseProgram(0);

	// Dibujamos la casa del té
	{
		// Activamos el shader estático
		phongShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", sunset.Color);
		phongShader->setVec4("LightPower", sunset.Power);
		phongShader->setInt("alphaIndex", sunset.alphaIndex);
		phongShader->setFloat("distance", sunset.distance);
		phongShader->setVec3("lightPosition", sunset.Position);
		phongShader->setVec3("lightDirection", sunset.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", wood.ambient);
		phongShader->setVec4("MaterialDiffuseColor", wood.diffuse);
		phongShader->setVec4("MaterialSpecularColor", wood.specular);
		phongShader->setFloat("transparency", wood.transparency);

		Casa_te->Draw(*phongShader);

	}

	glUseProgram(0);

	// Dibujamos al puente
	{
		// Activamos el shader estático
		phongShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", sunset_piso.Color);
		phongShader->setVec4("LightPower", sunset_piso.Power);
		phongShader->setInt("alphaIndex", sunset_piso.alphaIndex);
		phongShader->setFloat("distance", sunset_piso.distance);
		phongShader->setVec3("lightPosition", sunset_piso.Position);
		phongShader->setVec3("lightDirection", sunset_piso.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", wood.ambient);
		phongShader->setVec4("MaterialDiffuseColor", wood.diffuse);
		phongShader->setVec4("MaterialSpecularColor", wood.specular);
		phongShader->setFloat("transparency", wood.transparency);

		Puente->Draw(*phongShader);

	}

	// Dibujamos al piso exterior
	{
		// Activamos el shader estático
		phongShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", sunset_piso.Color);
		phongShader->setVec4("LightPower", sunset_piso.Power);
		phongShader->setInt("alphaIndex", sunset_piso.alphaIndex);
		phongShader->setFloat("distance", sunset_piso.distance);
		phongShader->setVec3("lightPosition", sunset_piso.Position);
		phongShader->setVec3("lightDirection", sunset_piso.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", wood.ambient);
		phongShader->setVec4("MaterialDiffuseColor", wood.diffuse);
		phongShader->setVec4("MaterialSpecularColor", wood.specular);
		phongShader->setFloat("transparency", wood.transparency);

		Piso_ext->Draw(*phongShader);

	}

	// Objeto estático Barda
	{
		// Activamos el shader del plano
		phongShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", sunset.Color);
		phongShader->setVec4("LightPower", sunset.Power);
		phongShader->setInt("alphaIndex", sunset.alphaIndex);
		phongShader->setFloat("distance", sunset.distance);
		phongShader->setVec3("lightPosition", sunset.Position);
		phongShader->setVec3("lightDirection", sunset.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", wood.ambient);
		phongShader->setVec4("MaterialDiffuseColor", wood.diffuse);
		phongShader->setVec4("MaterialSpecularColor", wood.specular);
		phongShader->setFloat("transparency", wood.transparency);

		Barda->Draw(*phongShader);
	}

	glUseProgram(0);

	// Objeto animado
	{
		// Activación del shader del personaje
		ourShader->use();

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		ourShader->setMat4("projection", projection);
		ourShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(12.0f,0.0f,-40.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.001f, 0.001f, 0.001f));	// it's a bit too big for our scene, so scale it down
		
		ourShader->setMat4("model", model);

		ourShader->setMat4("gBones", MAX_RIGGING_BONES, gBones);

		// Dibujamos el modelo
		character->Draw(*ourShader);
	}
	// Desactivamos el shader actual
	glUseProgram(0);


	// Pétalo animado
	{
		// Activación del shader del personaje
		phongShader->use();

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, -tradius * glm::tan(theta/2),0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		theta += 0.001;

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", W_light.Color);
		phongShader->setVec4("LightPower", W_light.Power);
		phongShader->setInt("alphaIndex", W_light.alphaIndex);
		phongShader->setFloat("distance", W_light.distance);
		phongShader->setVec3("lightPosition", W_light.Position);
		phongShader->setVec3("lightDirection", W_light.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", wood.ambient);
		phongShader->setVec4("MaterialDiffuseColor", wood.diffuse);
		phongShader->setVec4("MaterialSpecularColor", wood.specular);
		phongShader->setFloat("transparency", wood.transparency);
		

		// Dibujamos el modelo
		petalo->Draw(*phongShader);
	}
	// Desactivamos el shader actual
	glUseProgram(0);

	// Dibujamos sakura
	{
		// Activamos el shader estático
		phongShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", sunset.Color);
		phongShader->setVec4("LightPower", sunset.Power);
		phongShader->setInt("alphaIndex", sunset.alphaIndex);
		phongShader->setFloat("distance", sunset.distance);
		phongShader->setVec3("lightPosition", sunset.Position);
		phongShader->setVec3("lightDirection", sunset.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", W_light.Color);
		phongShader->setVec4("LightPower", W_light.Power);
		phongShader->setInt("alphaIndex", W_light.alphaIndex);
		phongShader->setFloat("distance", W_light.distance);
		phongShader->setVec3("lightPosition", W_light.Position);
		phongShader->setVec3("lightDirection", W_light.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", wall.ambient);
		phongShader->setVec4("MaterialDiffuseColor", wall.diffuse);
		phongShader->setVec4("MaterialSpecularColor", wall.specular);
		phongShader->setFloat("transparency", wall.transparency);

		sakura->Draw(*phongShader);

	}
	
	glUseProgram(0);

	// Dibujamos las 3 camas
	{
		// Activamos el shader estático
		staticShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		staticShader->setMat4("projection", projection);
		staticShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.1f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		staticShader->setMat4("model", model);

		cama_03->Draw(*staticShader);

	}

	glUseProgram(0);

	//// Dibujamos el conjuntoR
	//
	//{
	//	// Activamos el shader estático
	//	staticShader->use();

	//	// Activamos para objetos transparentes
	//	glEnable(GL_BLEND);
	//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//	// Aplicamos transformaciones de proyección y cámara (si las hubiera)
	//	glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
	//	glm::mat4 view = camera.GetViewMatrix();
	//	staticShader->setMat4("projection", projection);
	//	staticShader->setMat4("view", view);

	//	// Aplicamos transformaciones del modelo
	//	glm::mat4 model = glm::mat4(1.0f);
	//	model = glm::translate(model, glm::vec3(0.0f, 0.1f, 0.0f)); // translate it down so it's at the center of the scene
	//	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	//	model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
	//	staticShader->setMat4("model", model);

	//	conjuntoR->Draw(*staticShader);

	//}
	//
	//glUseProgram(0);

	// Dibujamos el conjuntoC

	{
		// Activamos el shader estático
		staticShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		staticShader->setMat4("projection", projection);
		staticShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.1f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		staticShader->setMat4("model", model);

		conjuntoC->Draw(*staticShader);

	}

	glUseProgram(0);

	// Dibujamos la maceta
	{
		// Activamos el shader estático
		staticShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		staticShader->setMat4("projection", projection);
		staticShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, positionP); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.4f, 0.4f, 0.4f));	// it's a bit too big for our scene, so scale it down
		staticShader->setMat4("model", model);
		//theta += 0.001;
		maceta->Draw(*staticShader);

	}

	glUseProgram(0);

	// Dibujamos el techo
	{
		// Activamos el shader estático
		phongShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", sunset.Color);
		phongShader->setVec4("LightPower", sunset.Power);
		phongShader->setInt("alphaIndex", sunset.alphaIndex);
		phongShader->setFloat("distance", sunset.distance);
		phongShader->setVec3("lightPosition", sunset.Position);
		phongShader->setVec3("lightDirection", sunset.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", wood.ambient);
		phongShader->setVec4("MaterialDiffuseColor", wood.diffuse);
		phongShader->setVec4("MaterialSpecularColor", wood.specular);
		phongShader->setFloat("transparency", wood.transparency);

		techo->Draw(*phongShader);

	}

	glUseProgram(0);

	// Dibujamos el paisaje
	{
		// Activamos el shader estático
		staticShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		staticShader->setMat4("projection", projection);
		staticShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));	// it's a bit too big for our scene, so scale it down
		staticShader->setMat4("model", model);

		paisaje->Draw(*staticShader);

	}

	glUseProgram(0);

	// Dibujamos el piso interior y el corredor
	{
		// Activamos el shader estático
		staticShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		staticShader->setMat4("projection", projection);
		staticShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		staticShader->setMat4("model", model);

		Pisos_int->Draw(*staticShader);

	}

	glUseProgram(0);

	// Dibujamos al banco y al bambú
	{
		// Activamos el shader estático
		staticShader->use();

		// Activamos para objetos transparentes
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		staticShader->setMat4("projection", projection);
		staticShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		staticShader->setMat4("model", model);

		Banco_B->Draw(*staticShader);

	}

	glUseProgram(0);

	//Dibujamos las paredes interiores
	{
		// Activamos el shader del plano
		phongShader->use();

		// Aplicamos transformaciones de proyección y cámara (si las hubiera)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 10000.0f);
		glm::mat4 view = camera.GetViewMatrix();
		phongShader->setMat4("projection", projection);
		phongShader->setMat4("view", view);

		// Aplicamos transformaciones del modelo
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, glm::vec3(2.0f, 2.0f, 2.0f));	// it's a bit too big for our scene, so scale it down
		phongShader->setMat4("model", model);

		// Configuramos propiedades de fuentes de luz
		phongShader->setVec4("LightColor", sunset.Color);
		phongShader->setVec4("LightPower", sunset.Power);
		phongShader->setInt("alphaIndex", sunset.alphaIndex);
		phongShader->setFloat("distance", sunset.distance);
		phongShader->setVec3("lightPosition", sunset.Position);
		phongShader->setVec3("lightDirection", sunset.Direction);
		phongShader->setVec3("eye", camera.Position);

		// Aplicamos propiedades materiales
		phongShader->setVec4("MaterialAmbientColor", wall.ambient);
		phongShader->setVec4("MaterialDiffuseColor", wall.diffuse);
		phongShader->setVec4("MaterialSpecularColor", wall.specular);
		phongShader->setFloat("transparency", wall.transparency);

		paredes->Draw(*phongShader);

	}

	glUseProgram(0);

	// Process audio
	cam_position.X = camera.Position.x;
	cam_position.Y = camera.Position.y;
	cam_position.Z = camera.Position.z;

	cam_Front.X = camera.Front.x;
	cam_Front.Y = camera.Front.y;
	cam_Front.Z = camera.Front.z;
	soundEngine->setListenerPosition(cam_position, cam_Front);
	
	// glfw: swap buffers 
	glfwSwapBuffers(window);
	glfwPollEvents();

	return true;
}

// Procesamos entradas del teclado
void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS)
		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);

	//----------------Restricciones al usuario---------------------
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
		if (position.z > BoundingBoxMin.z) {
			position.z = 0.0f;
		}
		else
			position = position + scaleV * forwardView;
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
		position = position - scaleV * forwardView;
		if (position.z < BoundingBoxMax.z) {
			position.z = -55.2f;
		}
		else
			position = position - scaleV * forwardView;
	}
	//------------------------------------------------------------


	//--------------------Rotar personaje-----------------------
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
		 rotateCharacter += 0.5f;

		glm::mat4 model = glm::mat4(1.0f);
		model = glm::rotate(model, glm::radians(rotateCharacter), glm::vec3(0.0f, 1.0f, 0.0f));
		glm::vec4 viewVector = model * glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
		forwardView = glm::vec3(viewVector);
		forwardView = glm::normalize(forwardView);
	}
	
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		rotateCharacter -= 0.5f;

		glm::mat4 model = glm::mat4(1.0f);
		model = glm::rotate(model, glm::radians(rotateCharacter), glm::vec3(0.0f, 1.0f, 0.0f));
		glm::vec4 viewVector = model * glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
		forwardView = glm::vec3(viewVector);
		forwardView = glm::normalize(forwardView);
	}
	//------------------------------------------------------------------------

	if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS)
	{
		rotateTable += 0.05f;
	}
	if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS)
	{
		rotateTable -= 0.05f;
	}
		
	//-----------------Volume O(+) L(-)---------------------------------
	if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
	{
		volume += 0.1f;
		if (volume<0.4f){
			cout<< volume << endl;
			snd->setVolume(volume);
		}
		else{
			volume = 0.4f;
			cout << volume << endl;
			snd->setVolume(0.4f);
		}
	}
	//===================Opciones de sonido===================================
	//				  (Teclado numérico)
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
	{
		//snd->stop();
		snd->setIsPaused(true);
		snd = soundEngine->play3D("audio/Track_1.mp3", position_1, false, true);
		snd->setVolume(volume);
		snd->setIsPaused(false);
	}

	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
	{
		//snd->stop();
		snd->setIsPaused(true);
		snd = soundEngine->play3D("audio/Track_2.mp3", position_2, false, true);
		snd->setVolume(volume);
		snd->setIsPaused(false);
	}

	if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
	{
		//snd->stop();
		snd->setIsPaused(true);
		snd = soundEngine->play3D("audio/Track_3.mp3", position_3, false, true);
		snd->setVolume(volume);
		snd->setIsPaused(false);
	}

	if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
	{
		//snd->stop();
		snd->setIsPaused(true);
		snd = soundEngine->play3D("audio/Track_4.mp3", position_4, false, true);
		snd->setVolume(volume);
		snd->setIsPaused(false);
	}

	if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS)
	{
		//snd->stop();
		snd->setIsPaused(true);
		snd = soundEngine->play3D("audio/Track_5.mp3", position_5, false, true);
		snd->setVolume(volume);
		snd->setIsPaused(false);
	}
	if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS)
	{
		//snd->stop();
		snd->setIsPaused(true);
		snd = soundEngine->play3D("audio/Minyo.mp3", position_5, false, true);
		snd->setVolume(volume);
		snd->setIsPaused(false);
	}
	if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS) {
		snd->setIsPaused(true);
		//snd->drop();
		//snd = 0;
		//recorder->startrecordingbufferedaudio();
		recorder->clearRecordedAudioDataBuffer();
	}
		
	if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS) {
		recorder->stopRecordingAudio();
	}
		
	if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS) {
		recorder->addSoundSourceFromRecordedAudio("myRecordedVoice");
		recordEngine->play2D("myRecordedVoice",false);
	}
	
	//-======================================================================

	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
	{
		volume -= 0.1f;
		if (volume > 0.0f) {
			cout << volume << endl;
			snd->setVolume(volume);
		}
		else {
			volume = 0.0f;
			cout << volume << endl;
			snd->setVolume(volume);
		}

	}

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		//if (button == true) {
		//	snd->setIsPaused(!true);
		//	button = !true;
		//	cout << button << endl;
		//}
		//if (button == !true) {
		//	snd->setIsPaused(true);
		//	button = true;
		//	cout << button << endl;
		//}
	}
	//-----------------------------------------------------------

	//------------------Gravedad para la vasija------------------
	if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS) {
		if (positionP.y > 0.0f) {
			force = mass_p * gravity;
			acceleration = force / mass_p;
			velocity += acceleration * deltaTime;
			positionP += velocity * deltaTime;
		}
		else {
			positionP = glm::vec3(20.0f, 0.0f, -9.0f);
		}
		//Hace caer al objeto
		
	}
	if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS) {
		positionP = glm::vec3(20.0f, 3.0f, -9.0f);
		//Vuelve a la posición original
	}
	//----------------------------------------------------------------

}

// glfw: Actualizamos el puerto de vista si hay cambios del tamaño
// de la ventana
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

// glfw: Callback del movimiento y eventos del mouse
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = (float)xpos;
		lastY = (float)ypos;
		firstMouse = false;
	}

	float xoffset = (float)xpos - lastX;
	float yoffset = lastY - (float)ypos; 

	lastX = (float)xpos;
	lastY = (float)ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: Complemento para el movimiento y eventos del mouse
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll((float)yoffset);
}
